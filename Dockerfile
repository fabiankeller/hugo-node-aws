FROM node:18

ENV HUGO_VERSION "0.110.0"

# install hugo
RUN mkdir /tmp/hugo \
    && cd /tmp/hugo \
    && wget "https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz" -O hugo.tgz \
    && tar xzf hugo.tgz \
    && chmod +x ./hugo \
    && cp ./hugo /usr/local/bin/ \
    && cd ~ \
    && rm -rf /tmp/hugo \
    && echo "Hugo Version:" \
    && hugo version

RUN apt-get update \
    && apt-get -y install python3-pip golang \
    && pip install awscli \
    && echo "AWS Version:" \
    && aws --version
